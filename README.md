# Totp Generator
Program to generate TOTP verification codes

## GPG commands for setup
If you're unfamiliar with GPG then I recommend to look for online guides. However, here are the commands you'll need to run the program: 

1. create new keypair:  
$ gpg --full-gen-key
2. encrypt file:  
$ gpg -e filename
3. export private key:  
$ gpg --export-secret-keys --armor keyIDNumber/email > secret.asc

## How to use
1. Create an application.properties file with following properties: 
   1. dir.path: path to directory with .gpg extension files which contain the TOTP secret code
   2. secret.file: gpg exported asc file to decrypt the TOTP files
