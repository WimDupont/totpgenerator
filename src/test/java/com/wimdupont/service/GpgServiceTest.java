package com.wimdupont.service;


import org.bouncycastle.openpgp.PGPException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;

import static org.mockito.Mockito.mockStatic;

@ExtendWith(MockitoExtension.class)
public class GpgServiceTest {

    private static final String TOTP_SECRET_FILE = "encrypted-totp-secret";
    private static final char[] PASSWORD = {'s', 'e', 'c', 'r', 'e', 't', 'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};
    private static String dirPath;
    private static String secretFile;

    private GpgService gpgService;
    @Mock
    private ApplicationProperties applicationProperties;

    @BeforeAll
    public static void setupClass() {
        var resourceDirectory = Paths.get("src", "test", "resources")
                .toAbsolutePath()
                .toString();
        dirPath = String.format("%s/totpfiles/", resourceDirectory);
        secretFile = String.format("%s/private-key.asc", resourceDirectory);
    }

    @BeforeEach
    public void setup() {
        gpgService = new GpgService(applicationProperties);
    }

    @Test
    public void decryptWithCorrectPasswordShouldReturnCorrectSecret() throws PGPException, IOException {
        Mockito.when(applicationProperties.getDirPath()).thenReturn(dirPath);
        Mockito.when(applicationProperties.getSecretFile()).thenReturn(secretFile);
        try (MockedStatic<PasswordReader> mockedPasswordReader = mockStatic(PasswordReader.class)) {
            mockedPasswordReader.when(PasswordReader::getPassword).thenReturn(PASSWORD);

            var result = gpgService.decrypt(TOTP_SECRET_FILE);

            Assertions.assertNotNull(result);
            Assertions.assertEquals("This is the big secret key", new String(result, Charset.defaultCharset()));
        }
    }

    @Test
    public void decryptWithWrongPasswordShouldThrowPgpException() {
        Mockito.when(applicationProperties.getDirPath()).thenReturn(dirPath);
        Mockito.when(applicationProperties.getSecretFile()).thenReturn(secretFile);
        try (MockedStatic<PasswordReader> mockedPasswordReader = mockStatic(PasswordReader.class)) {
            mockedPasswordReader.when(PasswordReader::getPassword).thenReturn(new char[]{'w', 'r', 'o', 'n', 'g'});

            Exception exception = Assertions.assertThrows(PGPException.class, () ->
                    gpgService.decrypt(TOTP_SECRET_FILE)
            );

            Assertions.assertEquals(PGPException.class, exception.getClass());
        }
    }

    @Test
    public void decryptUnknownFileNameArgShouldThrowException() {
        Mockito.when(applicationProperties.getDirPath()).thenReturn(dirPath);
        try (MockedStatic<PasswordReader> mockedPasswordReader = mockStatic(PasswordReader.class)) {
            mockedPasswordReader.when(PasswordReader::getPassword).thenReturn(PASSWORD);

            Exception exception = Assertions.assertThrows(FileNotFoundException.class, () ->
                    gpgService.decrypt("unexisting")
            );

            Assertions.assertEquals(FileNotFoundException.class, exception.getClass());
            Assertions.assertEquals("File \"unexisting.gpg\" not found", exception.getMessage());
        }
    }

    @Test
    public void decryptUnknownFileFromDirPathShouldThrowException() {
        Mockito.when(applicationProperties.getDirPath()).thenReturn("/wrong/path/");
        try (MockedStatic<PasswordReader> mockedPasswordReader = mockStatic(PasswordReader.class)) {
            mockedPasswordReader.when(PasswordReader::getPassword).thenReturn(PASSWORD);

            Exception exception = Assertions.assertThrows(FileNotFoundException.class, () ->
                    gpgService.decrypt(TOTP_SECRET_FILE)
            );

            Assertions.assertEquals(FileNotFoundException.class, exception.getClass());
            Assertions.assertEquals("File \"encrypted-totp-secret.gpg\" not found", exception.getMessage());
        }
    }

    @Test
    public void decryptUnknownFileFromSecretFileShouldThrowException() {
        Mockito.when(applicationProperties.getDirPath()).thenReturn(dirPath);
        Mockito.when(applicationProperties.getSecretFile()).thenReturn("/wrong/unexisting.asc");
        try (MockedStatic<PasswordReader> mockedPasswordReader = mockStatic(PasswordReader.class)) {
            mockedPasswordReader.when(PasswordReader::getPassword).thenReturn(PASSWORD);

            Exception exception = Assertions.assertThrows(FileNotFoundException.class, () ->
                    gpgService.decrypt(TOTP_SECRET_FILE)
            );

            Assertions.assertEquals(FileNotFoundException.class, exception.getClass());
        }
    }
}