package com.wimdupont;

import com.bastiaanjansen.otp.TOTPGenerator;
import com.wimdupont.service.ApplicationProperties;
import com.wimdupont.service.GpgService;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println(TOTPGenerator
                    .withDefaultValues(new GpgService(ApplicationProperties.getInstance())
                            .decrypt(getFileArg(args)))
                    .now());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(0);
        }
    }

    private static String getFileArg(String... args) {
        return args.length > 0
                ? args[0]
                : null;
    }
}
