package com.wimdupont.service;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.bouncycastle.openpgp.operator.bc.BcPublicKeyDataDecryptorFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.Iterator;

public class GpgService {

    private final ApplicationProperties applicationProperties;

    public GpgService(ApplicationProperties applicationProperties){
        this.applicationProperties = applicationProperties;
    }

    public byte[] decrypt(String fileName) throws IOException, PGPException {
        Security.addProvider(new BouncyCastleProvider());

        var encryptedDataObjects = getPgpEncryptedDataIterator(fileName);

        if (encryptedDataObjects.hasNext()) {
            var pgpPublicKeyEncryptedData = (PGPPublicKeyEncryptedData) encryptedDataObjects.next();
            var pgpPrivateKey = retrievePgpPrivateKey(pgpPublicKeyEncryptedData);

            return getByteArrayOutputStream(pgpPublicKeyEncryptedData, pgpPrivateKey);
        } else {
            throw new DataLengthException(String.format("No encrypted dataObjects found in file: %s", fileName));
        }
    }

    private Iterator<PGPEncryptedData> getPgpEncryptedDataIterator(String fileName) throws IOException {
        var secretDecoderStream = PGPUtil.getDecoderStream(KeyRetriever
                .getSecretKeyInputStream(fileName, applicationProperties.getDirPath()));
        var pgpObjectFactory = new PGPObjectFactory(secretDecoderStream, new BcKeyFingerprintCalculator());
        var nextObject = pgpObjectFactory.nextObject();

        return (nextObject instanceof PGPEncryptedDataList)
                ? ((PGPEncryptedDataList) nextObject).getEncryptedDataObjects()
                : ((PGPEncryptedDataList) pgpObjectFactory.nextObject()).getEncryptedDataObjects();
    }

    private PGPPrivateKey retrievePgpPrivateKey(PGPPublicKeyEncryptedData pgpPublicKeyEncryptedData) throws IOException, PGPException {
        var pgpPrivateKey = readSecretKey(new FileInputStream(applicationProperties.getSecretFile()), pgpPublicKeyEncryptedData.getKeyID())
                .extractPrivateKey(new BcPBESecretKeyDecryptorBuilder(new BcPGPDigestCalculatorProvider())
                        .build(PasswordReader.getPassword()));

        if (pgpPrivateKey == null) {
            throw new IllegalArgumentException("Secret key for message not found.");
        }

        return pgpPrivateKey;
    }

    private PGPSecretKey readSecretKey(InputStream inputStream,
                                       long keyId) throws IOException, PGPException {
        inputStream = PGPUtil.getDecoderStream(inputStream);
        var pgpSecretKeyRings = new PGPSecretKeyRingCollection(inputStream, new BcKeyFingerprintCalculator());
        var secretKey = pgpSecretKeyRings.getSecretKey(keyId);

        if (secretKey == null) {
            throw new IllegalArgumentException("Can't find encryption key in key ring.");
        }

        return secretKey;
    }

    private byte[] getByteArrayOutputStream(PGPPublicKeyEncryptedData pgpPublicKeyEncryptedData,
                                            PGPPrivateKey pgpPrivateKey) throws IOException, PGPException {
        ByteArrayOutputStream byteArrayOutputStream;

        try (var dataStream = toDataStream(pgpPublicKeyEncryptedData, pgpPrivateKey)) {
            byteArrayOutputStream = new ByteArrayOutputStream();
            int dataRead;

            while ((dataRead = dataStream.read()) >= 0) {
                byteArrayOutputStream.write(dataRead);
            }
        }

        return byteArrayOutputStream.toByteArray();
    }

    private InputStream toDataStream(PGPPublicKeyEncryptedData pgpPublicKeyEncryptedData,
                                     PGPPrivateKey pgpPrivateKey) throws PGPException, IOException {
        var pgpDataStream = pgpPublicKeyEncryptedData.getDataStream(
                new BcPublicKeyDataDecryptorFactory(pgpPrivateKey));
        var jcaPGPObjectFactory = new JcaPGPObjectFactory(pgpDataStream);
        var pgpCompressedDataStream = ((PGPCompressedData) jcaPGPObjectFactory.nextObject()).getDataStream();
        jcaPGPObjectFactory = new JcaPGPObjectFactory(pgpCompressedDataStream);

        return ((PGPLiteralData) jcaPGPObjectFactory.nextObject()).getDataStream();
    }

}
