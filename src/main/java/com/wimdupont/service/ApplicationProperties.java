package com.wimdupont.service;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.MissingResourceException;
import java.util.Properties;

public class ApplicationProperties {

    private static ApplicationProperties instance;
    private final String dirPath;
    private final String secretFile;

    public String getDirPath() {
        return dirPath;
    }

    public String getSecretFile() {
        return secretFile;
    }

    public static ApplicationProperties getInstance() {
        if (instance == null) {
            instance = new ApplicationProperties();
        }

        return instance;
    }

    private ApplicationProperties(){
        var properties = loadProperties();
        dirPath = properties.getProperty("dir.path");
        secretFile = properties.getProperty("secret.file");

        if (dirPath == null | secretFile == null) {
            throw new InvalidParameterException("Properties missing.");
        }
    }

    private Properties loadProperties() {
        final var properties = new Properties();
        var inputStream = getClass().getResourceAsStream("/application.properties");
        try {
            if (inputStream == null) {
                throw new IOException();
            }
            properties.load(inputStream);
        } catch (IOException e) {
            throw new MissingResourceException(
                    "Missing application properties",
                    Properties.class.getSimpleName(),
                    "application.properties");
        }
        return properties;
    }
}
