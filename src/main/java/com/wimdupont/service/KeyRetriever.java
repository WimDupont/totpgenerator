package com.wimdupont.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class KeyRetriever {

    private static final String GPG_EXTENSION = ".gpg";

    public static InputStream getSecretKeyInputStream(String fileArgument,
                                                      String dirPath) throws IOException {
        var scanner = new Scanner(System.in);
        File file;

        if (fileArgument != null) {
            file = new File(dirPath + fileArgument + GPG_EXTENSION).getAbsoluteFile();
            if (!file.exists()) {
                throw new FileNotFoundException(String.format("File \"%s\" not found", file.getName()));
            }
        } else {
            var files = new File(dirPath).listFiles();
            if (files == null || files.length < 1) {
                throw new FileNotFoundException(dirPath + " contains no files");
            }
            String fileNames = Arrays.stream(files)
                    .filter(f -> f.getName().contains(GPG_EXTENSION))
                    .map(f -> f.getName().replace(GPG_EXTENSION, ""))
                    .collect(Collectors.joining(", "));

            System.out.println("Which TOTP?");
            System.out.println(fileNames);

            String fileName = scanner.nextLine();

            file = Arrays.stream(files)
                    .filter(f -> f.getName().replace(GPG_EXTENSION, "").equals(fileName))
                    .findAny()
                    .orElseThrow(() -> new FileNotFoundException("File not found"));
        }

        return new FileInputStream(file.getAbsolutePath());
    }
}
