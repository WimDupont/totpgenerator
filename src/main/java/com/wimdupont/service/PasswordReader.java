package com.wimdupont.service;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import java.io.Console;

public class PasswordReader {

    public static char[] getPassword() {
        String prompt = "Enter password";
        Console console = System.console();
        return console == null
                ? getPasswordByPanel(prompt)
                : getPasswordByConsole(prompt, console);
    }

    private static char[] getPasswordByConsole(String prompt, Console console) {
        System.out.println(prompt);
        return console.readPassword();
    }

    private static char[] getPasswordByPanel(String prompt) {
        var panel = new JPanel();
        var passwordField = new JPasswordField(10);
        panel.add(new JLabel("Password"));
        panel.add(passwordField);
        var pane = new JOptionPane(panel, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION) {
            @Override
            public void selectInitialValue() {
                passwordField.requestFocusInWindow();
            }
        };
        pane.createDialog(null, prompt)
                .setVisible(true);

        return passwordField.getPassword().length == 0
                ? new char[0]
                : passwordField.getPassword();
    }
}
